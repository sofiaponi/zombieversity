package zombieversity;
import gameloop.GameLoop;
import javafx.application.Application;
import javafx.stage.Stage;

public class Game extends Application {

    public static void main(final String... args) {
            launch();
    }
    @Override
    public final void start(final Stage primaryStage) throws Exception {
        primaryStage.setTitle("Ci si prova");
        GameLoop loop = new GameLoop();
        loop.start();
        primaryStage.show();
    }
}
