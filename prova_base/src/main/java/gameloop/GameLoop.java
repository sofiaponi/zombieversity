package gameloop;

import java.awt.Canvas;

public class GameLoop extends Canvas implements Runnable {

    private static final long serialVersionUID = 1L;
    private boolean running = false;
    private Thread thread;

    public GameLoop() { 

    }

    public final synchronized void start() {
       if (this.running) {
           return;
       }
       this.running = true;
       this.thread = new Thread();
       this.thread.start();
    }

    public final synchronized void stop() {
        if (!this.running) {
            return;
        }
        this.running = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
                e.printStackTrace();
        }
    }

    @Override
    public final void run() {
        long lastTime = System.nanoTime();
        double amount = 60.0;
        double nsPerTick = 1000000000D / amount;
        double delta = 0;
        long timer = System.currentTimeMillis();

        int frames = 0;
        int ticks = 0;

        while (running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / nsPerTick;
            lastTime = now;

            while (delta >= 1) {
                ticks++;
                tick();
                delta -= 1;
            }

            try {
                Thread.sleep(2);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }

            frames++;
            render();

            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                System.out.println(ticks + " ticks, " + frames + " frames");
                frames = 0;
                ticks = 0;
            }
        }
        stop();
    }

    public final void tick() {

    }

    public void render() {

    }
}
