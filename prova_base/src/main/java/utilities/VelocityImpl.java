package utilities;

public class VelocityImpl implements Velocity {

    private final double velX;
    private final double velY;

    public VelocityImpl(final double velX, final double velY) {
        this.velX = velX;
        this.velY = velY;
    }

    public final double getModule() {
        return Math.sqrt(Math.pow(this.velX, 2) + Math.pow(this.velY, 2));
    }

    public final Velocity getNormalized() {
        return new VelocityImpl(this.velX / getModule(), this.velY / getModule());
    }

    public final double getVelX() {
        return this.velX;
    }

    public final double getVelY() {
        return this.velY;
    }

}
