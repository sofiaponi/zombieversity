package utilities;

public class PositionImpl implements Position {

    private final double x;
    private final double y;

    public PositionImpl(final double x, final double y) {
        this.x = x;
        this.y = y;
    }

    public final Position sum(final Velocity vel) {
        return new PositionImpl(this.x + vel.getVelX(), this.y + vel.getVelY());
    }

    public final double getX() {
        return this.x;
    }

    public final double getY() {
        return this.y;
    }

    public final String toString() {
        return "PositionImpl [x=" + x + ", y=" + y + "]";
    }

}
