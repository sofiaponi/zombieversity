package utilities;

public enum Direction {

    /**
     * 
     */
    UP(1), 
    /**
     * 
     */
    DOWN(-1),
    /**
     * 
     */
    LEFT(-1),
    /**
     * 
     */
    RIGHT(1), 
    /**
     * 
     */
    STOP(0);

    private final int valueDirection;

    Direction(final int valueDirection) {
       this.valueDirection = valueDirection;
    }
}
