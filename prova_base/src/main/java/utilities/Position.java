package utilities;

public interface Position {

    /**
     * @param vel 
     *           the velocity of the entity
     *
     * @return the new position.
     */
    Position sum(Velocity vel);

    double getX();

    double getY();

}
