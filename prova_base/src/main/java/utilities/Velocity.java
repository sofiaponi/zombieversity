package utilities;

public interface Velocity {

    double getModule();

    /**
     * 
     * @return the direction of the entity
     */
    Velocity getNormalized();

    double getVelX();

    double getVelY();
}
