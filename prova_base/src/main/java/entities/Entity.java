package entities;

import utilities.Position;
import utilities.Velocity;

public interface Entity {

    Position getPosition();

    void setPosition(Position pos);

    Velocity getVelocity();

    void setVelocity(Velocity vel);

}
