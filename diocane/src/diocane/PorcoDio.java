package diocane;

import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.Timer;

public class PorcoDio extends JFrame implements KeyListener, ActionListener{
	
	
	//tutto il resto model ??
	Timer t = new Timer(10,this);
	private int x = 300, y = 400;
	private int velX, velY;
	private int width = 30, height = 30;

	public PorcoDio() {
		super("Race");
	    setSize(800,600);
	    setVisible(true);
	    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    Container cont=getContentPane();
	    cont.setBackground(Color.pink);
	    setResizable(false);
	    addKeyListener(this);
	    t.start();
	}
	
	//Sarebbe la view ??
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.black);
		g.fillRect(x, y, width, height);
	}
	
	public void tick() {
		x += velX;
		y += velY;
	}

	//sarebbbe tipo il controller ???
	public void actionPerformed(ActionEvent e) {
		
		tick();
		
		repaint();
		
	}
	
	public void keyPressed(KeyEvent e) {
		
		switch(e.getKeyCode()) {
		case KeyEvent.VK_D:
			velX=1;
			break;
		case KeyEvent.VK_A:
			velX=-1;
			break;
		case KeyEvent.VK_S:
			velY=1;
			break;
		case KeyEvent.VK_W:
			velY=-1;
			break;
		}
		
	}



	@Override
	public void keyReleased(KeyEvent e) {
		switch(e.getKeyCode()) {
		case KeyEvent.VK_D:
			velX = 0;
			break;
		case KeyEvent.VK_A:
			velX = 0;
			break;
		case KeyEvent.VK_S:
			velY = 0;
			break;
		case KeyEvent.VK_W:
			velY = 0;
			break;
		}
		
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	

	public static void main(String args[]) {
		new PorcoDio();
	}

}
