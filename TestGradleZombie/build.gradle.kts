plugins {
	java
	application
}

repositories {
	jcenter()
}

val javaFXModules = listOf("base", "controls", "fxml", "swing", "graphics")

val supportedPlatforms = listOf("linux", "mac", "win")

dependencies {
	for (platform in supportedPlatforms){
		for(module in javaFXModules){
			implementation("org.openjfx:javafx-$module:13:$platform")
		}
	}
}

application {
	mainClassName = "controller.MyApplication"
}